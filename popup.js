// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
var storage = chrome.storage.local
/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}
function main(){
	
	setInterval(function() {
	var query = $.ajax('http://heroes3.tv/');
	query.done(function(html){
		console.log('YES SIR');
		ParseSite();
		
	})
	},60000);
}

function ParseSite(){
	var result = " ";
	var href = "";
	var JSONobj = JSON.parse(localStorage.getItem("savedData"));
	$("#av").remove();
	var parsed = JSONobj;
	z=parsed.length;
	// IF ANY STREAMS IS GET SHOW THEM IN THE LIST
	if (z>0){
			//Delete NO STREAMS AVAILABLE 
			$(parseInt(0)).remove();
			$('#streamList').empty();
			// var $list = $('#streamList li');
			// for (z; z<$list.length; z++){
				// $(parseInt(z)).remove
				// alert($list.length);
			// }
			
			for (var k in  parsed) {
				result = parsed[k].name;
				href = parsed[k].url;
				img = parsed[k].img;
				href="http://heroes3.tv"+href
				$('#streamList').append("<li id="+z+"> <img src=http://heroes3.tv/"+img +"><a href="+href+">"+result+"</a></li>")
			}
	}
}	


document.addEventListener('DOMContentLoaded', function() {
  ParseSite()
  main();
  getCurrentTabUrl(function(url) {
    }, function(errorMessage) {
      renderStatus('Cannot  connect to Heroes3.tv  ' + errorMessage);
    });
  });
### What is this repository for? ###

* This is Chrome Extension for checking if any streams on http://heroes3.tv/ are available.
* version 0.2

### How do I get set up? ###

Download it, open your chromium-based browser and pull "browser://extensions/" into your url field. Then, choose "add unpacked extensions" and in the file-tree window select the tab with this project.
To edit it just simply open it in any text-editor and enjoy your developing! 

### Who do I talk to? ###

e-mail: jummixe@gmail.com
skype: JumboMixe